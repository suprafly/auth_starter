defmodule AuthStarterWeb.UserRegistrationLiveTest do
  use AuthStarterWeb.ConnCase

  import Phoenix.LiveViewTest

  alias AuthStarter.Accounts
  alias AuthStarter.Accounts.User

  describe "tests against the user registration liveview" do
    test "sign up page renders", %{conn: conn} do
      {:ok, view, html} = live(conn, "/users/register")

      assert html =~ "Create an Account"
      assert render(view) =~ "Create an Account"

      assert view
             |> element("input#user_email")
             |> has_element?()

      assert view
             |> element("input#user_password")
             |> has_element?()

      assert view
             |> element("button", "Sign up")
             |> has_element?()
    end

    test "ensure validaton events work", %{conn: conn} do
      {:ok, view, _html} = live(conn, "/users/register")

      assert render_change(view, :validate, %{"user" => %{"email" => "someone"}}) =~
               "must have the @ sign and no spaces"

      assert render_change(view, :validate, %{"user" => %{"password" => "1"}}) =~
               "should be at least 12 character(s)"
    end

    test "successfully create a new user", %{conn: conn} do
      {:ok, view, html} = live(conn, "/users/register")

      assert html =~ "Create an Account"
      assert render(view) =~ "Create an Account"

      email = "someone@gmail.com"

      assert assert {:error, {:redirect, %{to: _}}} =
                      view
                      |> element("form")
                      |> render_submit(%{
                        "user" => %{"email" => email, "password" => "123456789012"}
                      })

      assert %User{email: ^email} = Accounts.get_user_by_email(email)
    end

    test "fail to create a new user", %{conn: conn} do
      {:ok, view, html} = live(conn, "/users/register")

      assert html =~ "Create an Account"
      assert render(view) =~ "Create an Account"

      assert view
             |> element("form")
             |> render_submit(%{"user" => %{"email" => "someone@gmail.com", "password" => "123"}}) =~
               "should be at least 12 character(s)"

      assert view
             |> element("form")
             |> render_submit(%{"user" => %{"email" => "someone", "password" => "123456789012"}}) =~
               "must have the @ sign and no spaces"
    end

    test "honeypot fields return a success but do not create user", %{conn: conn} do
      {:ok, view, html} = live(conn, "/users/register")

      assert html =~ "Create an Account"
      assert render(view) =~ "Create an Account"

      email = "someone@gmail.com"

      assert view
             |> element("form")
             |> render_submit(%{
               "user" => %{
                 "username" => "someone",
                 "email" => email,
                 "password" => "123456789012"
               }
             }) =~ "Your account was successfully created"

      assert Accounts.get_user_by_email(email) == nil

      email = "someone@gmail.com"

      assert view
             |> element("form")
             |> render_submit(%{
               "user" => %{
                 "email" => email,
                 "password" => "123456789012",
                 "confirm_password" => "123456789012"
               }
             }) =~ "Your account was successfully created"

      assert Accounts.get_user_by_email(email) == nil
    end
  end
end
