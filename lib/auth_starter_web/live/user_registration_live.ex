defmodule AuthStarterWeb.UserRegistrationLive do
  use AuthStarterWeb, :live_view

  alias AuthStarter.Accounts
  alias AuthStarter.Accounts.User

  @impl Phoenix.LiveView
  def mount(_params, _session, socket) do
    changeset = Accounts.change_user_registration(%User{})
    {:ok, assign(socket, :changeset, changeset)}
  end

  @impl Phoenix.LiveView
  def handle_event("validate", %{"user" => params}, socket) do
    changeset =
      %User{}
      |> User.registration_changeset(params)
      |> Map.put(:action, :insert)

    {:noreply, assign(socket, changeset: changeset)}
  end

  @impl Phoenix.LiveView
  def handle_event(
        "save",
        %{"user" => %{"confirm_password" => "", "username" => ""} = params},
        socket
      ) do
    socket =
      case Accounts.register_user(params) do
        {:ok, user} ->
          Accounts.deliver_user_confirmation_instructions(
            user,
            &Routes.user_confirmation_url(socket, :confirm, &1)
          )

          token =
            user
            |> Accounts.generate_user_session_token()
            |> Base.encode16()

          socket
          |> put_flash(:info, "Your account was successfully created")
          |> redirect(to: Routes.user_session_path(socket, :new, token: token))

        {:error, %Ecto.Changeset{} = changeset} ->
          assign(socket, changeset: changeset)
      end

    {:noreply, socket}
  end

  @impl Phoenix.LiveView
  def handle_event("save", _, socket) do
    # This is the  honeypot pathway - a bot has filled out a hidden field (username, or confirm_password)
    # Do what you want here. I will misdirect the bot to think that it has succeeded
    {:noreply,
     socket
     |> put_flash(:info, "Your account was successfully created")}
  end
end
