defmodule AuthStarterWeb.UserSessionController do
  use AuthStarterWeb, :controller

  alias AuthStarter.Accounts
  alias AuthStarterWeb.UserAuth

  def new(conn, %{"token" => token}) do
    decoded_token = Base.decode16!(token)

    if user = Accounts.get_user_by_session_token(decoded_token) do
      Accounts.delete_session_token(decoded_token)
      UserAuth.log_in_user(conn, user)
    else
      render(conn, "new.html", error_message: "Invalid email or password")
    end
  end

  def new(conn, _params) do
    render(conn, "new.html", error_message: nil)
  end

  def create(conn, %{"user" => user_params}) do
    %{"email" => email, "password" => password} = user_params

    if user = Accounts.get_user_by_email_and_password(email, password) do
      UserAuth.log_in_user(conn, user, user_params)
    else
      render(conn, "new.html", error_message: "Invalid email or password")
    end
  end

  def delete(conn, _params) do
    conn
    |> put_flash(:info, "Logged out successfully.")
    |> UserAuth.log_out_user()
  end
end
