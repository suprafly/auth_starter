defmodule AuthStarterWeb.LiveHelpers do
  @moduledoc """
  LiveView helpers.
  """
  import Phoenix.LiveView

  alias AuthStarter.Accounts

  def assign_defaults(%{"user_id" => user_id}, socket) do
    socket = assign_new(socket, :current_user, fn -> Accounts.get_user!(user_id) end)

    if socket.assigns.current_user.confirmed_at do
      socket
    else
      redirect(socket, to: "/login")
    end
  end
end
