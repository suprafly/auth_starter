defmodule AuthStarter.Repo do
  use Ecto.Repo,
    otp_app: :auth_starter,
    adapter: Ecto.Adapters.Postgres
end
