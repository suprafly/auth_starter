# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :auth_starter,
  ecto_repos: [AuthStarter.Repo]

# Configures the endpoint
config :auth_starter, AuthStarterWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "yZTMxi7ET8P2X+ZKanpdfiE7AzbEKHU2BPuyS3zv/NvW46RUoGem67NlaUqKq50k",
  render_errors: [view: AuthStarterWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: AuthStarter.PubSub,
  live_view: [signing_salt: "JTWDKkQK"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
